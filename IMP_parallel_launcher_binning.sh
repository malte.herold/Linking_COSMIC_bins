#!/bin/bash -l

#CMD_LAUNCH="oarsub --notify "mail:shaman.narayanasamy@uni.lu" -l core=12/nodes=1,walltime=72 -p "memnode='48'""
CMD_LAUNCH="oarsub -l walltime=24"
#CMD_LAUNCH="oarsub -t idempotent -t besteffort -l core=12/nodes=1,walltime=24"

#ROOTOUT="/work/projects/ecosystem_biology/COSMIC/IMP_output/Cosmic_new/MV_V1_V2_V3_samples"
ROOTOUT="/work/projects/ecosystem_biology/COSMIC/IMP_output_additionalMG/"

cat ${ROOTOUT}../IMP_output/Cosmic_new/Linking_cdhit975_additionalMG/Binning_redux.list | grep -v "^#" | while read DATA
do
  SAMPLE=$DATA
  INDIR="${ROOTOUT}/${SAMPLE}"
  OUTDIR="${ROOTOUT}/${SAMPLE}"
  
  CMD="./execution.sh $SAMPLE $INDIR"

  echo $CMD

  echo "$CMD_LAUNCH -n \"${SAMPLE}_binny\" \"$CMD\""

  $CMD_LAUNCH -n "${SAMPLE}_binny" "$CMD"

done
