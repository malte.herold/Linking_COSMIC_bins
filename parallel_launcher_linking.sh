#!/bin/bash -l

CMD_LAUNCH="oarsub -l walltime=5"


ROOTOUT="/mnt/nfs/projects/ecosystem_biology/COSMIC/IMP_output/Cosmic_new/Linking_cdhit975_additionalMG"

cat ${ROOTOUT}/samples.txt | grep -v "^#" | while read DATA

do
  PATIENT=$DATA
  INDIR="${ROOTOUT}/${PATIENT}"
  OUTDIR="${ROOTOUT}/${PATIENT}"
  
  SAMPLELIST=`dir $INDIR | tr "\t" "\n" | tr " " "\n" | grep -P "^[MV]" | tr "\n" " " `
  CMD="./execution_linking.sh \"$SAMPLELIST\" $INDIR $OUTDIR"

  echo $CMD

  echo "$CMD_LAUNCH -n \"${PATIENT}_linking\" \"$CMD\""

 # $CMD_LAUNCH -n "${PATIENT}_linking" "$CMD"

done
