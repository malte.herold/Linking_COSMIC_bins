# Workflow to link genomic bins in COSMIC samples

This documents the workflow used in the COSMIC study to link genomic bins 
recovered from assembly contigs produced by IMP (https://git-r3lab.uni.lu/IMP/IMP) v1.3. (https://webdav-r3lab.uni.lu/public/R3lab/IMP/dist/imp-1.3.tar.gz). 

The workflow was executed on the gaia HPC cluster and paths need to be adapted when this is executed elsewhere. 
A generic version of the workflow is planned for release at a later point in time. 

In short the workflow consists of 3 steps:
1. Preparation of input files
2. Recovering essential genes from the input 
3. Grouping the bins using essential gene sequence similarities

The workflow in this form was performed Oct 19th 2016. 


## 1. Preparation of input files

Samples need to be grouped by patient or independent "time-series".

For this, the script [symlinks_for_linking.sh](symlinks_for_linking.sh) is used for the COSMIC study.

The script is executed from the output-directory: `/mnt/nfs/projects/ecosystem_biology/COSMIC/IMP_output/Cosmic_new/Linking_cdhit975`

Input directories containing the samples:
1. `/mnt/nfs/projects/ecosystem_biology/COSMIC/IMP_output/Cosmic_new/MV_V1_V2_V3_samples`
2. `/mnt/nfs/projects/ecosystem_biology/COSMIC/IMP_output/Cosmic_new/M_samples`

The file `samples.txt` is written and contains a list of samples in this case the following list:
```
C100
C101
C106
C111
C112
C113
C114
C115
C116
C117
C118
C120
```

The script creates subdirectories for each sample and inside symbolic links to the IMP-output directories of the actual samples.
For `C116` links have to be created manually as the related mother samples are labelled C115. 

If binning has been performed for the assembly of contamination corrected input samples (the file `Analysis/clusteringWS_NOevil_Pooled_contigs.10.4.Rdata` exists) a symbolic link is created in the 
output-directory. 

## 2. Recovering essential genes from the input 

For each of the input samples the snakemake workflow [Binning_redux](workflows/Binning_redux) is performed.

To run this 2nd step of the workflow a [launcher script](IMP_parallel_launcher_binning.sh) is run twice, once for each set of input samples.
In both input sample directories (see above) a file `Binning_redux.list` lists the samples (# can be used to exclude single samples):

`MV_V1_V2_V3_samples/Binning_redux.list`:
```
MV_C101
MV_C106
MV_C111
MV_C112
MV_C114
MV_C115
MV_C117
MV_C120
V1_C111
V2_C100
V2_C101
V2_C106
V2_C111
V2_C112
V2_C115
V2_C116
V2_C117
V2_C120
V3_C100
V3_C101
V3_C106
V3_C111
V3_C112
V3_C113
V3_C115
V3_C116
V3_C117
V3_C118
V3_C120
```

`M_samples/Binning_redux.list`:
```
M_C100
M_C101
M_C106
M_C111
M_C114
M_C115
M_C117
M_C118
M_C120
```

The input assembly contigs have already been annotated with prokka and protein coding sequences (CDS)
have been predicted with prodigal. The predicted protein sequences are queried with a database of essential genes (`"%s/hmm/essential_genes/essential.hmm" % DBPATH`) in the rule
[find-essential-genes.rule](rules/Binning/find-essential-genes.rule).
The hmm-file can be found [here](https://github.com/MadsAlbertsen/multi-metagenome/blob/master/R.data.generation/essential.hmm)

In the following step the rule [separate-bins.rule](master/rules/Binning/separate-bins.rule) is applied extracting per-bin
contig sequences, protein sequences, and functional annotations.

The file `Binning/essMarkerGenes/markersAll.tsv` is generated with the rule [essential-gene-info.rule](rules/Binning/essential-gene_info.rule), 
while specific essential genes are extracted with the rule [extract-essential-genes.rule](rules/Binning/extract-essential-genes.rule).

The following list of essential genes are extracted this way:
```
TIGR01011
TIGR01049
TIGR01169
TIGR00487
TIGR01044
TIGR00959
PF00573.18
TIGR01171
PF00380.15
PF00297.18
TIGR00472
TIGR01067
TIGR01021
TIGR01050
TIGR01029
TIGR01164
PF00416.18
TIGR00468
TIGR01071
PF00276.16
PF00347.19
TIGR01632
PF00281.15
TIGR00981
TIGR00012
TIGR01009
PF00411.15
PF00466.16
PF00410.15
TIGR00060
TIGR00952
PF00366.16
TIGR01066
TIGR01079
TIGR02013
```

The list is based on a cross-section of the lists of essential single copy marker genes for bacteria published in the following articles:

```
Wu, D., Jospin, G., & Eisen, J. A. (2013). 
Systematic Identification of Gene Families for Use as “Markers” for Phylogenetic and Phylogeny-Driven Ecological Studies of Bacteria and Archaea and Their Major Subgroups. 
PLoS ONE, 8(10). http://doi.org/10.1371/journal.pone.0077033
```

```
Albertsen, M., Hugenholtz, P., Skarshewski, A., Nielsen, K. L., Tyson, G. W., & Nielsen, P. H. (2013). 
Genome sequences of rare, uncultured bacteria obtained by differential coverage binning of multiple metagenomes. 
Nature Biotechnology, 31(6), 533–538. http://doi.org/10.1038/nbt.2579
```

## 3. Grouping the bins using essential gene sequence similarities

For grouping bins based on essential gene protein sequence similarity another snakemake workflow is run: [TimeSeries_test](workflows/TimeSeries_test)
with another [launcher script](parallel_launcher_linking.sh).

The workflow consists of the following steps:

1. With the rule [concatenate-ess_genes.rule](rules/TimeSeries/concatenate-ess_genes.rule) all protein sequences that have been assigned to one of the specific marker genes (see list above) are concatenated.
2. With the rule [align_sequences.rule](rules/TimeSeries/align_sequences.rule) sequences are clustered with CD-HIT and table output constructed.
3. With the rule [link_clusters.rule](/rules/TimeSeries/link_clusters.rule) bins are assigned to linkage-groups in two steps:
    1. The script [link_cd_hit_groups.R](src/link_cd_hit_groups.R) is run in a first step to count how many times a bin occurs in a specific cluster of an essential marker gene. 
    2. The script [temporal_bin_groups_graph_based.R](src_needs_refinemenet/temporal_bin_groups_graph_based.R) uses the counts (co-occurence of marker genes of bins in a cd-hit cluster) to construct a weighted undirected graph. Highly interlinked subgroups are detected with the `cluster_fast_greedy` implementation in the igraph package and represent groups of similar bins (based on the sequence similarity of their marker genes) in different samples. 

The output files for the separate study subjects can be found under `/mnt/nfs/projects/ecosystem_biology/COSMIC/IMP_output/Cosmic_new/Linking_cdhit975`.

The file `graph_based_temporal_bins_0.975_fg_linkcountfiltered0.tsv` contains bin (column1) and group association (column2), while the file `groups_graph_based_0.975_fg_linkcountsfiltered0.pdf` contains 
a visualisation of the graph (edge weights are not shown, nodes represent bins, node color reflects group association).


# General Structure

## Configuration file
The configuration file [config_normalNode.json](config_normalNode.json) is the configuration file for IMP v.1.3, so it also contains parameters not needed for this analysis.
The parameters defined in the configuration file are default settings when parameters are not specified with environment variables, which are usually defined in the launcher scripts. (see below)

The configuration file could be reduced to the following parameters which are used within the workflow:
```
{
    "db_path": "/mnt/nfs/projects/ecosystem_biology/local_tools/IMP/dependencies/prokka/db",
    "binning": {
         "pk": 10,
         "nn": 4
     },
     "linking": {
         "cd_hit_perc": 0.975,
         "ts_topdir": "/work/projects/ecosystem_biology/COSMIC/IMP_output/Cosmic_new/MV_V1_V2_V3_samples/C100",
         "tb_outdir": "/work/projects/ecosystem_biology/COSMIC/IMP_output/Cosmic_new/MV_V1_V2_V3_samples/C100",
         "ts_sample_list": ["M_C100","MV_C100","V1_C100","V2_C100","V3_C100"],
         "marker_list": ["PF00276.16", "PF00281.15", "PF00297.18", "PF00347.19", "PF00366.16", "PF00380.15", "PF00410.15", "PF00411.15", "PF00416.18","PF00466.16", "PF00573.18", "TIGR00012", "TIGR00060", "TIGR00468","TIGR00472", "TIGR00487", "TIGR00952", "TIGR00959", "TIGR00981", "TIGR01009",   "TIGR01011", "TIGR01021", "TIGR01029", "TIGR01044", "TIGR01049", "TIGR01050","TIGR01066", "TIGR01067", "TIGR01071", "TIGR01079", "TIGR01164", "TIGR01169", "TIGR01171", "TIGR01632", "TIGR02013"],
         "filter_bins": "FALSE",
         "filter_groups": "FALSE",
         "edge_weight": 0
     }
}
```

## Launcher Scripts

The launcher scripts for steps 2. and 3. in the workflow schedule jobs on the gaia hpc to run the given steps for the specified samples.
These launcher scripts in turn call "execution scripts" which load [dependencies](src/preload_modules.sh) and execute a snakemake workflow.
Step 2. is incorporated in the [Snakefile](Snakefile) while the workflow for step 3. is executed with `snakemake -s workflows/TimeSeries_test` in the respective script.


# Dependencies
Dependencies are loaded on gaia with an [accompanying script](/blob/master/src/preload_modules.sh), which also loads more than the needed modules for this workflow, which only requires the following:

* pullseq
* HMMER3
* BioPerl
* CD-HIT (used: v4.6.1-2012-08-27_OpenMP) , (clstr_sql_tbl.pl needs to be available in the $PATH)
* R (used 3.0.2-ictce-5.3.0), R modules are loaded from a local library with: `.libPaths("/work/users/mherold/REPOS/AOB_TS_binning/r_libs")` which includes (among others) the following packages:
    * RColorBrewer
    * igraph
    * ggplot2
    * stringr
    * ggnet
    * network
    * sna
* additional note: Prokka v.1.11 which is part of IMP v1.3 is required or adjustment of the locus_tag naming convention should be taken into account (features need to have locus_tags in the format: PROKKA_123)
