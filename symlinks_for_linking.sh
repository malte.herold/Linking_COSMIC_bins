# from Cosmic_new/Linking

cat ../M_samples/Binning_redux.list ../MV_V1_V2_V3_samples/Binning_redux.list | cut -d "_" -f 2 | sort | uniq | parallel "mkdir -p {}"

cat ../M_samples/Binning_redux.list ../MV_V1_V2_V3_samples/Binning_redux.list | cut -d "_" -f 2 | sort | uniq >samples.txt

p=`pwd`

for s in `cat samples.txt`
do
    cd $s
    echo $PWD
    parallel "if [ -f {}/Analysis/clusteringWS_NOevil_Pooled_contigs.10.4.Rdata ]; then ln -s {} {/};fi" ::: ../../MV_V1_V2_V3_samples/*_$s 
    if [ -d ../../M_samples/M_$s ];then

        parallel "if [ -f {}/Analysis/clusteringWS_NOevil_Pooled_contigs.10.4.Rdata ]; then ln -s {} {/};fi" ::: ../../M_samples/*_$s
    fi
    cd $p
done 


