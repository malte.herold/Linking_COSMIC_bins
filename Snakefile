## Input and output parameters
INPUTDIR = os.environ.get("INPUTDIR")
OUTPUTDIR = os.environ.get("OUTPUTDIR")
TMPDIR = os.environ.get("TMPDIR", "/tmp")
SAMPLE = os.environ.get("SAMPLE")
SRCDIR = os.environ.get("SRCDIR", "src")
#SRCDIR_REF = os.environ.get("SRCDIR_REF", "src_needs_refinemenet")
CONFIG = os.environ.get("CONFIG", "./config_normalNode.json")
DBPATH = os.environ.get("DBPATH", "/mnt/nfs/projects/ecosystem_biology/local_tools/IMP/dependencies/prokka/db")

configfile: CONFIG

MEMCORE = os.environ.get("MEMCORE", config['memory_per_core_gb'])
THREADS = os.environ.get("THREADS", config['threads'])
MEMTOTAL = os.environ.get("MEMTOTAL", config['memory_total_gb'])

workdir:
    OUTPUTDIR

include:
    "workflows/Binning_redux"

#include:
#    "workflows/Remapping"
#

#include:
#    "workflows/TimeSeries_test"
#
#include:
#    "workflows/Refinement_Bins"

# master command
rule ALL:
    input:
        "binning.done",
	   # "linking_bins.done"
    output:
        touch('workflow.done')
