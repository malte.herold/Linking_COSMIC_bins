#!/bin/bash -l

### This script takes in the necessary arguments for Snakemake and attaches them into the
### appropriate Snakemake command.

source src/preload_modules.sh

date
#  SAMPLE=${1} INPUTDIR=${2} OUTPUTDIR=${2} snakemake --cleanup-metadata Binning/ORFS.hmmer.essential.out Binning/ORFS.hmm.orfs.essential.hits Binning/ORFS.hmm.orfs.essential.hits_reduced Binning/ORFS-contig_links.bed Binning/ORFS.hmm.orfs.essential.hits.protids Binning/ORFS.hmm.orfs.essential.hits.faa 

#  SAMPLE=${1} INPUTDIR=${2} OUTPUTDIR=${2} snakemake --touch
#  SAMPLE=${1} INPUTDIR=${2} OUTPUTDIR=${2} snakemake -nf binning.done

#  SAMPLE=${1} INPUTDIR=${2} OUTPUTDIR=${2} snakemake -rpf binning.done
  TS_SAMPLES=${1} TS_DIR=${2} TB_OUTDIR=${2} snakemake -ps workflows/TimeSeries_test
date
